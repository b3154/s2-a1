package com.zuitt.example;
import  java.util.Scanner;
public class LeapYear {
    public static void main(String[] args) {

        int year = 0;

        System.out.print("Input a year to be checked:");
        Scanner myObj= new Scanner(System.in);
        year = myObj.nextInt();

        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is not a leap year.");
        }

     }
}
